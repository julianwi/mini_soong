# Builders for C and C++
#
# Copyright 2020 Andrej Shadura
#
# SPDX-License-Identifier: MIT
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


__all__ = [
    'cc_defaults',
    'cc_binary',
    'cc_binary_host',
    'cc_library',
    'cc_library_shared',
    'cc_library_host_shared',
    'cc_library_static',
    'cc_library_host_static',
    'cc_test',
    'cc_test_host',
    'cc_benchmark',
    'cc_benchmark_host',
    'art_cc_defaults',
    'art_cc_binary',
    'art_cc_binary_host',
    'art_cc_library',
    'art_cc_library_shared',
    'art_cc_library_host_shared',
    'art_cc_library_static',
    'art_cc_static_library',
    'art_cc_library_host_static',
    'art_cc_test',
    'art_cc_test_host',
    'art_cc_benchmark',
    'art_cc_benchmark_host',
    'libart_cc_defaults',
    'gensrcs',
    'genrule',
]

from ..utils import mergedefaults, print_vars, match_libs
from ..model import defaults, all_targets, all_modules, current_recipe
from functools import lru_cache
from pathlib import Path
import os

targets_binary = list()
targets_shlib = list()

flag_blacklist = ['-Werror', '-U_FORTIFY_SOURCE', '-m32', '-m64', '-Wno-#pragma-messages']

host_libraries = ['liblz4', 'liblzma', 'libcrypto', 'libz']

@lru_cache(maxsize=None)
def detect_arch():
    if 'DEB_HOST_ARCH' in os.environ:
        return os.environ['DEB_HOST_ARCH']
    else:
        return 'amd64'

def map_arch():
    arch = detect_arch()
    if arch == 'amd64':
        return 'x86_64'
    elif arch == 'i386':
        return 'x86'
    elif arch == 'arm64':
        return 'arm64'
    elif arch.startswith('arm'):
        return 'arm'
    elif arch.startswith('mips64'):
        return 'mips64'
    elif arch.startswith('mips'):
        return 'mips'
    return arch

def multilib():
    if map_arch().endswith('64'):
        return 'lib64'
    else:
        return 'lib32'

@lru_cache(maxsize=None)
def detect_multiarch():
    if 'DEB_HOST_MULTIARCH' in os.environ:
        return os.environ['DEB_HOST_MULTIARCH']
    else:
        return 'x86_64-linux-gnu'

def collect_defaults(args):
    global defaults
    def_cxxflags = []
    def_cflags = []
    def_ldflags = []
    def_ldlibs = []
    def_shared_libs = []
    def_srcs = []
    def_include_dirs = []
    def_export_include_dirs = []
    def_generated_sources = []
    for default in args.get('defaults', []):
        if default in defaults:
            def_cxxflags += [f"$({default}_CXXFLAGS)"]
            def_cflags += [f"$({default}_CFLAGS)"]
            def_ldflags += [f"$({default}_LDFLAGS)"]
            def_ldlibs += [f"$({default}_LDLIBS)"]
            def_shared_libs += [f"$({default}_SHARED_LIBS)"]
            def_srcs += [f"$({default}_SRCS)"]
            def_include_dirs += [f"$({default}_INCLUDE_DIRS)"]
            def_export_include_dirs += [f"$({default}_EXPORT_INCLUDE_DIRS)"]
            def_generated_sources += [f"$({default}_GENERATED_SOURCES)"]
    target_specific = args.get('target', {}).get('linux_glibc', {})
    mergedefaults(args, target_specific)
    host_specific = args.get('target', {}).get('host', {})
    mergedefaults(args, host_specific)
    unix_specific = args.get('target', {}).get('not_windows', {})
    mergedefaults(args, unix_specific)
    linux_specific = args.get('target', {}).get('linux', {})
    mergedefaults(args, linux_specific)
    multilib_specific = args.get('multilib', {}).get(multilib(), {})
    mergedefaults(args, multilib_specific)

    codegen_arches = args.get('codegen', {})
    for arch in codegen_arches:
        mergedefaults(codegen_arches[arch], codegen_arches[arch].get('shared', {}))
    for val_type in ['srcs', 'shared_libs', 'host_ldlibs']:
        if codegen_arches.get('x86', {}).get(val_type):
            mergedefaults(codegen_arches, {'x86_64': {val_type: [f"$({args['name']}_x86_CODEGEN_{val_type.upper()})"]}})
        if codegen_arches.get('arm', {}).get(val_type):
            mergedefaults(codegen_arches, {'arm64': {val_type: [f"$({args['name']}_arm_CODEGEN_{val_type.upper()})"]}})

    for arch, value in codegen_arches.items():
        if value.get('srcs'):
            value['srcs'] = [(src if '*' not in src else f"$(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/{src}))") for src in value['srcs']]
        print_vars(args['name'] + '_' + arch + '_CODEGEN', value, ['srcs', 'shared_libs'])
    if codegen_arches:
        def_srcs += [f"$({args['name']}_$(HOST_ARCH)_CODEGEN_SRCS)"]
        def_shared_libs += [f"$({args['name']}_$(HOST_ARCH)_CODEGEN_SHARED_LIBS)"]

    arches = args.get('arch', {})
    for arch, value in arches.items():
        if value.get('srcs'):
            value['generated_sources'] = [f"$({src[1:]}_GENERATED_SOURCES)" for src in value['srcs'] if src.startswith(':')]
            value['srcs'] = [src for src in value['srcs'] if not src.startswith(':')]
        print_vars(args['name'] + '_' + arch, value, ['srcs', 'generated_sources', 'host_ldlibs'])
    if arches:
        def_srcs += [f"$({args['name']}_$(HOST_ARCH)_SRCS)"]
        def_generated_sources += [f"$({args['name']}_$(HOST_ARCH)_GENERATED_SOURCES)"]
        def_ldlibs += [f"$({args['name']}_$(HOST_ARCH)_HOST_LDLIBS)"]
    return def_cxxflags, def_cflags, def_ldflags, def_ldlibs, def_shared_libs, def_srcs, def_include_dirs, def_export_include_dirs, def_generated_sources

def filter_flags(flags):
    return [flag for flag in flags if flag not in flag_blacklist]

def cc_defaults(**args):
    def_cxxflags, def_cflags, def_ldflags, def_ldlibs, def_shared_libs, def_srcs, def_include_dirs, def_export_include_dirs, def_generated_sources = collect_defaults(args)
    defaults[args['name']] = {k: v for k, v in args.items() if k != 'name'}
    local_include_dirs = args.get('local_include_dirs', [])
    cxxflags = filter_flags(def_cxxflags + args.get('cppflags', []) + [f"-I{inc}" for inc in local_include_dirs])
    cflags = filter_flags(def_cflags + args.get('cflags', []) + [f"-I{inc}" for inc in local_include_dirs])
    ldflags = filter_flags(def_ldflags + args.get('ldflags', []))
    shared_libs = args.get('shared_libs', [])
    shared_libs += args.get('shared', {}).get('shared_libs', {})
    ldlibs = def_ldlibs + [f"-l{lib[3:]}" for lib in shared_libs if lib in host_libraries]
    ldlibs += [f"-l{lib[3:]}" for lib in args.get('static_libs', []) if lib in host_libraries]
    ldlibs += [f"-l{lib[3:]}" for lib in args.get('whole_static_libs', []) if lib in host_libraries]
    ldlibs += args.get('host_ldlibs', [])
    shared_libs = [lib for lib in shared_libs if lib not in host_libraries]
    srcs = def_srcs + args.get('srcs', [])
    srcs = [(src if '*' not in src else f"$(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/{src}))") for src in srcs]

    include_dirs = def_include_dirs + args.get('include_dirs', [])
    if 'generated_headers' in args:
        include_dirs += ['$(LOCAL_PATH)/generated']
    export_include_dirs = def_export_include_dirs + ["$(LOCAL_PATH)/"+d for d in args.get('export_include_dirs', [])]
    export_system_include_dirs = args.get('export_system_include_dirs', [])
    system_includes = [f"-isystem {inc}" for inc in export_system_include_dirs]
    generated_sources = def_generated_sources + [f"$({gen}_GENERATED_SOURCES)" for gen in args.get('generated_sources', [])]
    if "cpp-define-generator-asm-support" in args.get('generated_headers', []):
        print("""define cpp-define-generator-asm-support_GENERATED_SOURCES
include art/build/Android.common_build.mk
GENERATED_SRC_DIR = $(call local-generated-sources-dir)

$$(GENERATED_SRC_DIR)/asm_defines.S: PRIVATE_CUSTOM_TOOL = $(HOST_CXX) -S $$< -o $$@ $(HOST_GLOBAL_CPPFLAGS) $(HOST_GLOBAL_CFLAGS) $(ART_HOST_CFLAGS) $(addprefix -I , $(ART_C_INCLUDES) libbase/include libnativehelper/include/nativehelper)
$$(GENERATED_SRC_DIR)/asm_defines.S : art/tools/cpp-define-generator/asm_defines.cc
	$$(transform-generated-source)

$$(GENERATED_SRC_DIR)/asm_defines.h: PRIVATE_CUSTOM_TOOL = art/tools/cpp-define-generator/make_header.py $$< > $$@
$$(GENERATED_SRC_DIR)/asm_defines.h : $$(GENERATED_SRC_DIR)/asm_defines.S
	$$(transform-generated-source)
LOCAL_GENERATED_SOURCES += $$(GENERATED_SRC_DIR)/asm_defines.h
endef""")
        generated_sources += ["$(cpp-define-generator-asm-support_GENERATED_SOURCES)"]
    print_vars(args['name'], locals(), ['cxxflags', 'cflags', 'ldflags', 'ldlibs', 'shared_libs', 'srcs', 'includes', 'system_includes', 'include_dirs', 'export_include_dirs', 'generated_sources'])
    print()

def cc_compile_link(args, binary: bool = True, shared: bool = False, variables: bool = True):
    print(f"# link {args['name']} {'binary' if binary else ('shared' if shared else 'static') + ' library'}")

    def_cxxflags, def_cflags, def_ldflags, def_ldlibs, def_shared_libs, def_srcs, def_include_dirs, def_export_include_dirs, def_generated_sources = collect_defaults(args)
    local_include_dirs = args.get('local_include_dirs', [])
    if not binary:
        export_c_include_dirs = def_export_include_dirs + args.get('export_include_dirs', [])
        export_system_include_dirs = args.get('export_system_include_dirs', [])
        system_includes = [f"-isystem {inc}" for inc in export_system_include_dirs]
    cxxflags = filter_flags(def_cxxflags + args.get('cppflags', []) + [f"-I{inc}" for inc in local_include_dirs])
    cflags = filter_flags(def_cflags + args.get('cflags', []) + [f"-I{inc}" for inc in local_include_dirs])
    if not binary:
        if shared and 'shared' in args:
            mergedefaults(args, args['shared'])
        elif not shared and 'static' in args:
            mergedefaults(args, args['static'])

    ldflags = filter_flags(def_ldflags + args.get('ldflags', []))
    shared_libs = args.get('shared_libs', []) + def_shared_libs
    static_libs = args.get('static_libs', [])
    whole_static_libs = args.get('whole_static_libs', [])
    ldlibs = def_ldlibs
    ldlibs += args.get('host_ldlibs', [])
    ldlibs = def_ldlibs + [f"-l{lib[3:]}" for lib in shared_libs if lib in host_libraries]
    shared_libs = [lib for lib in shared_libs if lib not in host_libraries]
    shlibdeps = []
    c_includes = def_include_dirs + args.get('include_dirs', [])

    src_files_ = def_srcs + args.get('srcs', [])
    src_files = ["$(sort $(LOCAL_SRC_FILES_))"]  # remove duplicates
    shared_libraries = shared_libs
    static_libraries = static_libs
    whole_static_libraries = whole_static_libs
    generated_sources = def_generated_sources + [f"$({gen}_GENERATED_SOURCES)" for gen in args.get('generated_sources', [])]
    cpp_extension = args.get('cpp_extension', [])
    cppflags = cxxflags
    print("include $(CLEAR_VARS)")
    print(f"LOCAL_MODULE := {args['name']}")
    print(f"LOCAL_MODULE_CLASS := {'EXECUTABLES' if binary else 'SHARED_LIBRARIES' if shared else 'STATIC_LIBRARIES'}")
    print(f"LOCAL_IS_HOST_MODULE := true")
    if variables:
        print_vars("LOCAL", locals(), ['cppflags', 'cflags', 'ldflags', 'ldlibs', 'src_files_', 'src_files', 'shared_libraries', 'static_libraries', 'whole_static_libraries', 'export_c_include_dirs', 'c_includes', 'cpp_extension'])
    for gen in generated_sources:
        print(f"$(eval {gen})")

    if binary:
        print("include $(BUILD_HOST_EXECUTABLE)")
    elif shared:
        print("include $(BUILD_HOST_SHARED_LIBRARY)")
    else:
        print("include $(BUILD_HOST_STATIC_LIBRARY)")
    print()

def cc_binary(**args):
    cc_compile_link(args, binary = True, shared = False)

def cc_binary_host(**args):
    cc_compile_link(args, binary = True, shared = False)

def cc_library(**args):
    cc_compile_link(args.copy(), binary = False, shared = True)
    cc_compile_link(args, binary = False, shared = False)

def cc_library_shared(**args):
    cc_compile_link(args, binary = False, shared = True)

def cc_library_host_shared(**args):
    cc_compile_link(args, binary = False, shared = True)

def cc_library_static(**args):
    cc_compile_link(args, binary = False, shared = False)

def cc_library_host_static(**args):
    cc_compile_link(args, binary = False, shared = False)

def cc_test(**args):
    pass

def cc_test_host(**args):
    pass

def cc_benchmark(**args):
    pass

def cc_benchmark_host(**args):
    pass

def have_cxx(files):
    return any(is_cxx(f) for f in files)

def is_cxx(filename):
    return (filename.endswith('.cc') or
            filename.endswith('.cxx') or
            filename.endswith('.cpp') or
            filename.endswith('.CPP') or
            filename.endswith('.c++') or
            filename.endswith('.cp') or
            filename.endswith('.C'))

def flag_defaults():
    print("DPKG_EXPORT_BUILDFLAGS = 1")
    print("-include /usr/share/dpkg/buildflags.mk\n")
    print("CXXFLAGS += " + ' '.join([
        "-D__STDC_FORMAT_MACROS",
        "-D__STDC_CONSTANT_MACROS",
        "-std=c++11",
    ]))
    print("CFLAGS += " + ' '.join([
        "-D_FILE_OFFSET_BITS=64",
        "-D_LARGEFILE_SOURCE=1",
        "-Wa,--noexecstack",
        "-fPIC",
        "-fcommon",
    ]))
    print("LDFLAGS += " + ' '.join([
        "-Wl,-z,noexecstack",
        "-Wl,--no-undefined-version",
        "-Wl,--as-needed",
    ]))
    print("LDLIBS += " + ' '.join([
        f'-l{lib}' for lib in [
            "c",
            "dl",
            "gcc",
            #"gcc_s",
            "m",
            #"ncurses",
            "pthread",
            #"resolv",
            "rt",
            "util",
        ]
    ]))
    print()

def extra_targets():
    targets = []
    if targets_binary:
        print(f"install-binaries: install-{' install-'.join(targets_binary)}")
        targets.append('install-binaries')

    if targets_shlib:
        print(f"install-shlibs: install-{' install-'.join(targets_shlib)}")
        targets.append('install-shlibs')

    return targets

art_cc_defaults = cc_defaults
libart_cc_defaults = cc_defaults
art_defaults = {"cflags": ["$(ART_HOST_CFLAGS)"], "include_dirs": ["$(ART_C_INCLUDES)"], "cpp_extension": [".cc"]}

def art_cc_binary(**args):
    print("include art/build/Android.common_build.mk")
    mergedefaults(args, art_defaults)
    cc_binary(**args)

def art_cc_binary_host(**args):
    print("include art/build/Android.common_build.mk")
    mergedefaults(args, art_defaults)
    cc_binary_host(**args)

def art_cc_library(**args):
    print("include art/build/Android.common_build.mk")
    mergedefaults(args, art_defaults)
    cc_library(**args)

def art_cc_library_static(**args):
    print("include art/build/Android.common_build.mk")
    mergedefaults(args, art_defaults)
    cc_library_static(**args)

art_cc_static_library = art_cc_library_static
art_cc_library_shared = cc_library_shared
art_cc_library_host_shared = cc_library_host_shared
art_cc_library_host_static = cc_library_host_static
art_cc_test = cc_test
art_cc_test_host = cc_test_host
art_cc_benchmark = cc_benchmark
art_cc_benchmark_host = cc_benchmark_host

def gensrcs(**args):
    # local-generated-sources-dir method needs LOCAL_MODULE to be set. Therefore, we delay evaluation and evaluate it with $(eval) later.
    cmd = args.get('cmd', "")
    cmd = cmd.replace("$(location generate-operator-out.py)", "art/tools/generate-operator-out.py")
    cmd = cmd.replace("$(location generate_operator_out)", "art/tools/generate_operator_out.py")
    cmd = cmd.replace("$(in)", "$$<")
    cmd = cmd.replace("$(out)", "$$@")
    print_vars(args['name'], args, ['srcs'])
    print(f"define {args['name']}_GENERATED_SOURCES")
    print("GENERATED_SRC_DIR = $(call local-generated-sources-dir)")
    print(f"{args['name']}_GEN = $$(addprefix $$(GENERATED_SRC_DIR)/,$(addsuffix _{args.get('output_extension', [])},$({args['name']}_SRCS)))")
    print()
    print(f"$$({args['name']}_GEN): PRIVATE_CUSTOM_TOOL = {cmd}")
    print(f"$$({args['name']}_GEN): $$(GENERATED_SRC_DIR)/%_{args.get('output_extension', [])} : $(LOCAL_PATH)/%")
    print("\t$$(transform-generated-source)")
    print(f"LOCAL_GENERATED_SOURCES += $$({args['name']}_GEN)")
    print()
    print("endef")
    print()

def genrule(**args):
    # local-generated-sources-dir method needs LOCAL_MODULE to be set. Therefore, we delay evaluation and evaluate it with $(eval) later.
    cmd = args.get('cmd', "")
    cmd = cmd.replace("$(location interpreter/mterp/gen_mterp.py)", "art/runtime/interpreter/mterp/gen_mterp.py")
    cmd = cmd.replace("$(in)", f"$(LOCAL_PATH)/$({args['name']}_SRCS)")
    cmd = cmd.replace("$(out)", "$$@")
    print_vars(args['name'], args, ['srcs',  'out'])
    print(f"define {args['name']}_GENERATED_SOURCES")
    print("GENERATED_SRC_DIR = $(call local-generated-sources-dir)")
    print(f"{args['name']}_GEN = $$(addprefix $$(GENERATED_SRC_DIR)/,$({args['name']}_OUT))")
    print()
    print(f"$$({args['name']}_GEN): PRIVATE_CUSTOM_TOOL = {cmd}")
    print(f"$$({args['name']}_GEN):")
    print("\t$$(transform-generated-source)")
    print(f"LOCAL_GENERATED_SOURCES += $$({args['name']}_GEN)")
    print()
    print("endef")
    print()